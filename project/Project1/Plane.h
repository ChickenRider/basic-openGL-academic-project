#pragma once
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
// Other Libs
#include <SOIL.h>

#include "Shader.h"
#include "Model.h"


class Plane {
public:
	Plane() {
		setupPlane();
	}
	Vertex vertices[4] = {
		{ glm::vec3(-5000.0f,-0.5f,-5000.0f) ,glm::vec3(0.0f,1.0f,0.0f) ,glm::vec2(0.0f,0.0f) },
		{ glm::vec3(-5000.0f,-0.5f,5000.0f) ,glm::vec3(0.0f,1.0f,0.0f) ,glm::vec2(0.0f,250.0f) },
		{ glm::vec3(5000.0f,-0.5f,-5000.0f) ,glm::vec3(0.0f,1.0f,0.0f) ,glm::vec2(250.0f,0.0f) },
		{ glm::vec3(5000.0f,-0.5f,5000.0f) ,glm::vec3(0.0f,1.0f,0.0f) ,glm::vec2(250.0f,250.0f) }
	};
	GLuint indices[6] = {  
		3, 2, 1,  
		2, 0, 1    
	};

	GLuint texture;

	// Render the plane
	void Draw(Shader shader)
	{
		glUniform1i(glGetUniformLocation(shader.Program, "material.diffuseLoaded"), true);
		glUniform1i(glGetUniformLocation(shader.Program, "material.specularLoaded"), false);

		glUniform1i(glGetUniformLocation(shader.Program, "material.diffuse"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);

		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}



private:
	GLuint VAO, VBO, EBO;
	void setupPlane()
	{
		// Create buffers/arrays
		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &this->VBO);
		glGenBuffers(1, &this->EBO);

		glBindVertexArray(this->VAO);
		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
		glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);


		// Vertex Positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
		// Vertex Normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
		// Vertex Texture Coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

		glBindVertexArray(0);

		//texture

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture); // All upcoming GL_TEXTURE_2D operations now have effect on our texture object
												// Set our texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);	// Set texture wrapping to GL_REPEAT
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
		// Set texture filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Load, create texture and generate mipmaps
		int width, height;
		unsigned char* image = SOIL_load_image("../Resources/Textures/Grass.jpg", &width, &height, 0, SOIL_LOAD_RGB);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(image);
		glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.
	}
};
