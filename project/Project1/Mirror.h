#pragma once

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include "Shader.h"
#include "Model.h"

#include "Init_Main.h"
#include "Camera.h"

class Mirror{
public:
	glm::vec3 pos = { 8.0f,0.0f,0.0f };
	glm::vec3 size = { 1.0f,1.0f,1.0f };
	glm::vec3 rot = { 0.0f, 90.0f, 0.0f };

	Mirror(glm::vec3 Position) {
		pos = Position;
		setupMirror();
	}


	glm::mat4 portal_view(glm::mat4 orig_view) {
		glm::mat4 portal_cam = orig_view
			* glm::rotate(glm::mat4(1.0), glm::radians(180.0f), glm::vec3(0.0, 1.0, 0.0))
			* glm::inverse(getModelTransform())
		;
		portal_cam = glm::scale(portal_cam, { -1,1,1 });

		return portal_cam;
	}
	void DrawAlternate(Shader shader) {
		glUniform1i(glGetUniformLocation(shader.Program, "material.diffuseLoaded"), false);
		glUniform1i(glGetUniformLocation(shader.Program, "material.specularLoaded"), false);


		glm::mat4 model = glm::mat4();
		model = glm::translate(model, pos);
		model = glm::scale(model, size);

		model = glm::rotate(model, glm::radians(rot.y), glm::vec3(0.0, 1.0, 0.0));
		model = glm::rotate(model, glm::radians(rot.x), glm::vec3(1.0, 0.0, 0.0));
		model = glm::rotate(model, glm::radians(rot.z), glm::vec3(0.0, 0.0, 1.0));

		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));

		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
	void Draw(Shader shader, GLuint texture) {
		glUniform1i(glGetUniformLocation(shader.Program, "material.diffuseLoaded"), true);
		glUniform1i(glGetUniformLocation(shader.Program, "material.specularLoaded"), false);

		glUniform1i(glGetUniformLocation(shader.Program, "material.diffuse"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glm::mat4 model = glm::mat4();
		model = glm::translate(model, pos);
		model = glm::scale(model, size);

		model = glm::rotate(model, glm::radians(rot.y), glm::vec3(0.0, 1.0, 0.0));
		model = glm::rotate(model, glm::radians(rot.x), glm::vec3(1.0, 0.0, 0.0));
		model = glm::rotate(model, glm::radians(rot.z), glm::vec3(0.0, 0.0, 1.0));

		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));


		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

private:

	Vertex vertices[4] = {
		{ glm::vec3(0.0f,-6.0f,-3.0f) ,glm::vec3(1.0f,0.0f,0.0f) ,glm::vec2(0.0f,0.0f) },
		{ glm::vec3(0.0f,-6.0f,3.0f) ,glm::vec3(1.0f,0.0f,0.0f) ,glm::vec2(1.0f,1.0f) },
		{ glm::vec3(0.0f,6.0f,-3.0f) ,glm::vec3(1.0f,0.0f,0.0f) ,glm::vec2(0.0f,1.0f) },
		{ glm::vec3(0.0f,6.0f,3.0f) ,glm::vec3(1.0f,0.0f,0.0f) ,glm::vec2(1.0f,1.0f) }
	};
	GLuint indices[6] = {
		3, 2, 1,
		2, 0, 1
	};
	float shininess = 8.0f;

	GLuint VAO, VBO, EBO;


	glm::mat4 getModelTransform() {
		glm::mat4 model = glm::mat4();
		return model;
	}

	void setupMirror()
	{
		// Create buffers/arrays
		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &this->VBO);
		glGenBuffers(1, &this->EBO);

		glBindVertexArray(this->VAO);
		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
		glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

		// Vertex Positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
		// Vertex Normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
		// Vertex Texture Coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

		glBindVertexArray(0);
	}

};

