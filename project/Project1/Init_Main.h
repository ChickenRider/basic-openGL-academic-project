#pragma once

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
#include <iostream>

#include "Camera.h"

namespace Init {

	//Important stuff
	int renderWidth, renderHeight;
	int WindowWidth = 1600;
	int WindowHeight = 900;
	char* WindowName = "Zadanie4";

	// Cameras
	Camera cameraStatic(glm::vec3(-10.0f, 20.0f, -35.0f));//0
	Camera cameraTracking(glm::vec3(10.0f, 15.0f, -25.0f));//1
	Camera cameraFollowing(glm::vec3(0.0f, 100.0f, 0.0f));//2
	int chosenCamera = 0;
	Camera* currentCamera = &cameraStatic;

	//Light
	bool SpotLight1On = true;
	bool SpotLight2On = true;
	bool Blinn = true;
	int ShadingModel = 2; //0-flat, 1-Gourard, 2-Phong

	//FUNCTIONS

	//input
	bool keys[1024];
	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

	void cameraSetUp();
	void cameraSwitch();
	void cameraHandle(glm::vec3 trackedObjectPosition, glm::vec3 trackedObjectMovementDirection);

	GLFWwindow* WindowInit() {
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		//making window and having it as context
		GLFWwindow* window = glfwCreateWindow(WindowWidth, WindowHeight, WindowName, nullptr, nullptr);
		if (window == nullptr)
		{
			std::cout << "Failed to create GLFW window" << std::endl;
			glfwTerminate();
			return nullptr;
		}
		glfwMakeContextCurrent(window);

		// Set the required callback functions
		glfwSetKeyCallback(window, key_callback);

		//initializing glew
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
		{
			std::cout << "Failed to initialize GLEW" << std::endl;
			return nullptr;
		}
		
		//tell OpenGL the size of the rendering window 
		
		glfwGetFramebufferSize(window, &renderWidth, &renderHeight);
		glViewport(0, 0, renderWidth, renderHeight);
		// Setup some OpenGL options
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_STENCIL_TEST);
		//glEnable(GL_CULL_FACE);

		cameraSetUp();

		return window;
	}

#pragma region "User input"

	// Is called whenever a key is pressed/released via GLFW
	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GL_TRUE);
		if (key == GLFW_KEY_C && action == GLFW_PRESS)
			cameraSwitch();
		if (key == GLFW_KEY_A && action == GLFW_PRESS)
			ShadingModel = (ShadingModel + 1) % 3;
		if (key == GLFW_KEY_S && action == GLFW_PRESS)
			SpotLight1On = 1 - SpotLight1On;
		if (key == GLFW_KEY_D && action == GLFW_PRESS)
			SpotLight2On = 1 - SpotLight2On;
		if (key == GLFW_KEY_B && action == GLFW_PRESS)
			Blinn = 1 - Blinn;
		if (action == GLFW_PRESS)
			keys[key] = true;
		else if (action == GLFW_RELEASE)
			keys[key] = false;
	}

	void cameraSetUp() {
		cameraStatic.Track({ 0.0f, 0.0f, 0.0f });
	}

	void cameraSwitch() {
		chosenCamera = (chosenCamera + 1) % 3;
		switch (chosenCamera)
		{
		case 0:
			currentCamera = &cameraStatic;
			break;
		case 1:
			currentCamera = &cameraTracking;
			break;
		case 2:
			currentCamera = &cameraFollowing;
			break;
		default:
			break;
		}
	}
	
	void cameraHandle(glm::vec3 trackedObjectPosition, glm::vec3 trackedObjectMovementDirection) {
		if (chosenCamera == 1)
			currentCamera->Track(trackedObjectPosition);
		else if (chosenCamera == 2) {
			glm::vec3 dir = glm::normalize(trackedObjectMovementDirection);
			currentCamera->Position = glm::vec3(trackedObjectPosition.x - trackedObjectMovementDirection.x * 100.0f, trackedObjectPosition.y + 10.0f, trackedObjectPosition.z - trackedObjectMovementDirection.z * 100.0f);
			currentCamera->Track(trackedObjectPosition);
		}
	}
#pragma endregion

}