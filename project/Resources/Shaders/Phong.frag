#version 330 core
struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
	
	vec3 ambient;
	bool diffuseLoaded;
	bool specularLoaded;
}; 

struct SpotLight {
    vec3  position;
    vec3  direction;
    float cutOff;
	float outerCutOff;
	
	float strength;
    float constant;
    float linear;
    float quadratic;
	
    vec3 color;
	bool on;
};

struct PointLight {
    vec3 position;
    
	float strength;
    float constant;
    float linear;
    float quadratic;
	
    vec3 color;
};

#define NR_SPOT_LIGHTS 2

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 color;

uniform vec3 viewPos;
uniform vec3 ambient;
uniform SpotLight spotLights[NR_SPOT_LIGHTS];
uniform PointLight pointLight;
uniform Material material;
uniform bool Blinn; //lighting model (Blinn/Phong)

// Function prototypes
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 textureDiffuse, vec3 textureSpecular);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 textureDiffuse, vec3 textureSpecular);

void main()
{    
    // Properties
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);
    
	//Phase 0: Ambient lighting
	vec3 textureDiffuse;
	float alpha = 1.0;
	if(material.diffuseLoaded) {
		vec4 tmp = vec4(texture(material.diffuse, TexCoords));
		alpha = tmp.w;
		if(alpha < 0.1)
			discard;
		textureDiffuse = vec3(tmp);
	}
	else {
		textureDiffuse = material.ambient;
	}
	vec3 result = ambient * textureDiffuse;
	
	vec3 textureSpecular;
	
	if(material.specularLoaded)
		textureSpecular = vec3(texture(material.specular, TexCoords));
	else
		textureSpecular = vec3(1.0,1.0,1.0);
	
    // Phase 1: Point lighting
    result += CalcPointLight(pointLight, norm, FragPos, viewDir, textureDiffuse, textureSpecular);
	
    // Phase 2: Spot lights
    for(int i = 0; i < NR_SPOT_LIGHTS; i++)
		if(spotLights[i].on)
			result += CalcSpotLight(spotLights[i], norm, FragPos, viewDir, textureDiffuse, textureSpecular);
    
    color = clamp(vec4(result, 1.0), 0.0, 1.0);
}

// Calculates the color when using a spot light.
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 textureDiffuse, vec3 textureSpecular)
{
    vec3 lightDir = normalize(light.position - fragPos);
    
    // Check if lighting is inside the spotlight cone
    float theta = dot(lightDir, normalize(-light.direction)); 
    float epsilon = (light.cutOff - light.outerCutOff);
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    
    if(intensity > 0)
    {    
        // Diffuse       
        float diff = max(dot(normal, lightDir), 0.0);
        
        // Specular
        vec3 reflectDir = reflect(-lightDir, normal);  
        float spec;
		if(Blinn == true) 
		{
			vec3 H = (normalize(lightDir) + normalize(viewDir))/2;
			spec = pow(max(dot(normal, H), 0.0), material.shininess);
		}
		else
			spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
			
			
        vec3 diffuse = diff * textureDiffuse * intensity;  
        vec3 specular = spec * textureSpecular * intensity;
        
		
        // Attenuation
        float distance = length(light.position - fragPos);
        float attenuation = light.strength / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    

        
        diffuse  *= attenuation;
        specular *= attenuation;   
                
		diffuse = clamp(diffuse, 0.0, 1.0);
		specular = clamp(specular, 0.0, 1.0);
				
        return light.color * (diffuse + specular);  
    }
    else 
        return 0;
}

// Calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 textureDiffuse, vec3 textureSpecular)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
	
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec;
        
	if(Blinn == true) 
	{
		vec3 H = (normalize(lightDir) + normalize(viewDir))/2;
		spec = pow(max(dot( H, normal), 0.0), material.shininess);
	}
	else
		spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	
    vec3 diffuse = diff * textureDiffuse;
    vec3 specular = spec * textureSpecular;
	
	// Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = light.strength / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	
	diffuse  *= attenuation;
    specular *= attenuation;  
	
	diffuse = clamp(diffuse, 0.0, 1.0);
	specular = clamp(specular, 0.0, 1.0);
	
	return light.color * (diffuse + specular);
}